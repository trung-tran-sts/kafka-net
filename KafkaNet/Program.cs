﻿using Confluent.Kafka;
using Confluent.Kafka.Admin;
using System;
using System.Threading.Tasks;

namespace KafkaNet
{
    class Program
    {
        const string LocalKafkaServer = "172.24.119.106:9092";

        static async Task Main(string[] args)
        {
            await StartAdminTasks();
        }

        static async Task StartAdminTasks()
        {
            var config = new AdminClientConfig()
            {
                BootstrapServers = LocalKafkaServer // bootstrap.servers
            };
            var builder = new AdminClientBuilder(config);
            IAdminClient adminClient = builder.Build();
            await adminClient.CreateTopicsAsync(new[]
            {
                new TopicSpecification()
                {
                    Name = "quote-feedback"
                }
            });
        }
    }
}
